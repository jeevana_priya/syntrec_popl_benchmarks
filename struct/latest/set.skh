//@Description This library contains several set related methods

struct Set {
	int n;
	Node head;
}

struct Node {
	int val;
	Node next;	
}

Set new_set() {
	return new Set();	
}

Set set_add(Set s, int v) {
	int len = s.n;
	if (len == 0) {
		s.head = new Node(val = v);
		s.n = len + 1;
		return s;
	} else {
		Node cur = s.head;
		Node prev = null;
		while (cur != null) {
			if (v < cur.val) {
				Node n = new Node(val = v, next = cur);	
				if (prev == null) {
					s.head = n;
				} else {
					prev.next = n;
				}
				s.n = len + 1;
				return s;
			} else if (v == cur.val) {
				return s;	
			} else {
				prev = cur;
				cur = cur.next;
			}
		}
		Node n = new Node(val = v);
		prev.next = n;
		s.n = len + 1;
		return s;
	}
}

Set set_addAll(Set s1, Set s2) {
	if (s1.n == 0) return s2;
	if (s2.n == 0) return s1;
	Node cur = s2.head;
	while (cur != null) {
		s1 = set_add(s1, cur.val);
		cur = cur.next;	
	}	
	return s1;
}

Set set_remove(Set s, int v) {
	int len = s.n;
	if (len == 0) {
		return s;
	} else {
		Node cur = s.head;
		Node prev = null;
		while (cur != null) {
			if (v == cur.val) {
				if (prev == null) {
					s.head = cur.next;
				} else {
					prev.next = cur.next;
				}
				s.n = len - 1;
				return s;
			} else {
				prev = cur;
				cur = cur.next;
			}
		}
		return s;
	}
}

int set_size(Set s) {
	return s.n;	
}

bit set_contains(Set s, int v) {
	int len = s.n;
	if (len == 0) {
		return false;
	} else {
		Node cur = s.head;
		while (cur != null) {
			if (v == cur.val) {
				return true;
			} else {
				cur = cur.next;
			}
		}
		return false;
	}
}

@replaceable("")
bit set_equals(Set s1, Set s2) {
	int l1 = s1.n, l2 = s2.n;
	if (l1 != l2) return false;
	if (l1 == 0) return true;
	Node cur1 = s1.head;
	Node cur2 = s2.head;
	while (cur1 != null) {
		assert(cur2 != null);
		if (cur1.val != cur2.val) return false;
		cur1 = cur1.next;
		cur2 = cur2.next;	
	}
	return true;
}

struct UnorderedSet {
	int n;
	Node head;
}

UnorderedSet new_unordered_set() {
	return new UnorderedSet();	
}

UnorderedSet unordered_set_insert(UnorderedSet s, int v) {
	int len = s.n;
	Node n = new Node(val = v, next = s.head);
	s.head = n;
	s.n = len + 1;
	return s;
}

UnorderedSet unordered_set_join(UnorderedSet s, UnorderedSet s1) {
	if (s1.n == 0) return s;
	if (s.n == 0) return s1;
	int len = s.n;
	Node cur = s1.head;
	Node prev = null;
	while(cur != null) {
		prev = cur;
		cur = cur.next;	
	}
	prev.next = s.head;
	s.head = s1.head;
	s.n = len + s1.n;
	return s;
}

UnorderedSet unordered_set_remove(UnorderedSet s, int v) {
	int len = s.n;
	if (len == 0) {
		return s;
	} else {
		Node cur = s.head;
		Node prev = null;
		while (cur != null) {
			if (v == cur.val) {
				if (prev == null) {
					s.head = cur.next;
				} else {
					prev.next = cur.next;
				}
				s.n = len - 1;
				return s;
			} else {
				prev = cur;
				cur = cur.next;
			}
		}
		return s;
	}
}

UnorderedSet unordered_set_remove_first(UnorderedSet s) {
	int len = s.n;
	if (len == 0) {
		return s;
	} else {
		s.head = s.head.next;
		s.n = len - 1;
		return s;
	}
}


int unordered_set_size(UnorderedSet s) {
	return s.n;	
}

bit unordered_set_contains(UnorderedSet s, int v) {
	int len = s.n;
	if (len == 0) {
		return false;
	} else {
		Node cur = s.head;
		while (cur != null) {
			if (v == cur.val) {
				return true;
			} else {
				cur = cur.next;
			}
		}
		return false;
	}
}

@replaceable("")
bit unordered_set_equals(UnorderedSet s1, UnorderedSet s2) {
	int l1 = s1.n, l2 = s2.n;
	if (l1 != l2) return false;
	if (l1 == 0) return true;
	Node cur1 = s1.head;
	Node cur2 = s2.head;
	while (cur1 != null) {
		assert(cur2 != null);
		if (cur1.val != cur2.val) return false;
		cur1 = cur1.next;
		cur2 = cur2.next;	
	}
	return true;
}

bit unordered_set_is_sorted(UnorderedSet s) {
	if (s.n == 0) return true;
	Node cur = s.head.next;
	int prevVal = s.head.val;
	while(cur != null) {
		if (cur.val > prevVal) {
			return false;
		}
		prevVal = cur.val;
		cur = cur.next;
	}
	return true;	
}

UnorderedSet unordered_set_insert_ordered(UnorderedSet s, int v){
	// Assumes that is_sorted is already tested
	int len = s.n;
	if (len == 0) {
		s.head = new Node(val = v);
		s.n = len + 1;
		return s;
	} else {
		Node cur = s.head;
		Node prev = null;
		while (cur != null) {
			if (v >= cur.val) {
				Node n = new Node(val = v, next = cur);	
				if (prev == null) {
					s.head = n;
				} else {
					prev.next = n;
				}
				s.n = len + 1;
				return s;
			} else {
				prev = cur;
				cur = cur.next;
			}
		}
		Node n = new Node(val = v);
		prev.next = n;
		s.n = len + 1;
		return s;
	}
}