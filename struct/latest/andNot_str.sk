pragma options "--bnd-inline-amnt 3"; 
harness void andNotOptim(int[2] assignment) {
	Dag original = new Dag (type = And, and_a= new Dag (type = Var, id = 0, vtype = Bit), and_b = new Dag (type = Not, not_a = new Dag (type = Var, id = 1, vtype = Bit)));
	Dag optNode = optimize(original, assignment);
	assert(equals(run(original, assignment), run(optNode, assignment)));
}

int Bit = 1;
int Int = 0;

int Equal =2;
int Plus = 3;
int Lt = 4;
int Gt = 5;
int Var = 6;
int Num = 7;
int And = 8;
int Or = 9;
int Not = 10;
int Mux = 11;
int Bool = 12;


struct Dag {
	int type;
	Dag eq_a;
	Dag eq_b;
	Dag plus_a;
	Dag plus_b;
	Dag lt_a;
	Dag lt_b;
	Dag gt_a;
	Dag gt_b;
	Dag and_a;
	Dag and_b;
	Dag or_a;
	Dag or_b;
	Dag not_a;
	Dag mux_v;
	Dag mux_a;
	Dag mux_b;
	int id;
	bit vtype;
	int val;
	bit x;
}

generator int getType() { return {|Equal | Plus| Lt| Gt| Var| Num| And| Or| Not| Mux | Bool|}; }

generator Dag getDag (int depth) {
	if (??) {
		return {| new Dag(type = Num, val = ??) | new Dag(type = Bool, x = ??) | new Dag(type = Var, id = ??, vtype = ??) |};
	} else 
		if (depth > 1) {
			Dag eq_a= getDag(depth - 1);
	Dag eq_b = getDag(depth - 1);
	Dag plus_a = getDag(depth - 1);
	Dag plus_b = getDag(depth - 1);
	Dag lt_a =  getDag(depth - 1);
	Dag lt_b = getDag(depth - 1);
	Dag gt_a = getDag(depth - 1);
	Dag gt_b =  getDag(depth - 1);
	Dag and_a = getDag(depth - 1);
	Dag and_b = getDag(depth - 1);
	Dag or_a = getDag(depth - 1);
	Dag or_b = getDag(depth - 1);
	Dag not_a = getDag(depth - 1);
	Dag mux_v = getDag(depth - 1);
	Dag mux_a = getDag(depth - 1);
	Dag mux_b = getDag(depth - 1);
			return new Dag(type = getType(), eq_a = eq_a, eq_b = eq_b, plus_a = plus_a, plus_b = plus_b, lt_a = lt_a, lt_b = lt_b, gt_a = gt_a, gt_b = gt_b, and_a = and_a, and_b = and_b, or_a = or_a, or_b = or_b, mux_v = mux_v, mux_a = mux_a, mux_b = mux_b, id = ??, vtype = ??, val = ??, x = ??);
		} 
	
}

Dag optimize (Dag original, int[2] assignment) {
	// Find a random predicate
	Dag predicate = getDag(3);
	// Make sure that there exists atleast one input that satisfies the predicate
	int[2] tmp = ??;
	
	assert( check(predicate, tmp) );
	
	if (check(predicate, assignment)) {
		// Assignment satisfies the predicate
		Dag optimized = getDag(3);
		// Make sure that the optimized version is actually smaller
		int c = count(optimized);
		assert(c < count(original));
		return optimized;
	} 
	return original;
}



Dag run([int n], Dag p, int[n] assignment) {
	if (p == null) return null;
	
		if (p.type == Equal) {
			Dag a = run(p.eq_a, assignment);
			Dag b = run(p.eq_b, assignment);
			assert(a.type == Num);
			assert(b.type == Num);
			return new Dag (type = Bool, x = a.val  == b.val );
		}	
		
		else if (p.type == Plus) {
			Dag a = run(p.plus_a, assignment);
			Dag b = run(p.plus_b, assignment);
			assert(a.type == Num);
			assert(b.type == Num);
			return new Dag (type = Num, val =  a.val + b.val);
		} 
		
		else if (p.type == Lt) {
			Dag a = run(p.lt_a, assignment);
			Dag b = run(p.lt_b, assignment);
			assert(a.type == Num);
			assert(b.type == Num);
			return new Dag (type = Bool, x = a.val < b.val);
		}
		
		else if (p.type == Gt) {
			Dag a = run(p.gt_a, assignment);
			Dag b = run(p.gt_b, assignment);
			assert(a.type == Num);
			assert(b.type == Num);
			return new Dag(type = Bool, x = a.val > b.val);
		}
		
		else if (p.type == Var) {
			assert p.id < n;
			if (p.vtype == Int) {
				return new Dag (type = Num, val = assignment[p.id]);
			} else {
				return new Dag (type = Bool, x = assignment[p.id] == 1);
			}
		}
		else if (p.type == And)  {
			Dag a =  run(p.and_a, assignment);
			Dag b =  run(p.and_b, assignment);
			assert (a.type == Bool);
			assert (b.type == Bool);
			return new Dag (type = Bool, x = a.x && b.x);
		}
		else if (p.type == Or){
			Dag a =  run(p.or_a, assignment);
			Dag b =  run(p.or_b, assignment);
			assert (a.type == Bool);
			assert (b.type == Bool);
			return new Dag (type = Bool, x = a.x || b.x);
		}
		else if (p.type == Not) {
			Dag a =  run(p.not_a, assignment);
			assert(a.type == Bool);
			return new Dag (type = Bool, x = !a.x);
		}
		else if (p.type == Mux) {
			Dag v = run(p.mux_v, assignment);
			assert(v.type == Bool);
			return v.x ?  run(p.mux_a, assignment) : run(p.mux_b, assignment);
		}
		else return p;
	

}

int count(Dag d) {
	if (d == null) return 0;
	
		if (d.type == Equal){
			int a = count(d.eq_a);
			int b = count(d.eq_b);
			return a + b +1;
		}
		else if (d.type == Plus) {
			int a = count(d.plus_a);
			int b = count(d.plus_b);
			return a + b + 1;
		}
		else if (d.type == Lt) {
			int a = count(d.lt_a);
			int b = count(d.lt_b);
			return a + b + 1;
		}
		else if (d.type == Gt) {
			int a = count(d.gt_a);
			int b = count(d.gt_b);
			return a + b + 1;
		}
		else if (d.type == And) {
			int a = count(d.and_a);
			int b = count(d.and_b);
			return a + b + 1;
		}
		else if (d.type == Or) {
			int a = count(d.or_a);
			int b = count(d.or_b);
			return a + b + 1;
		}
		else if (d.type == Not) {
			int a = count(d.not_a);
			return a + 1;
		}
		else if (d.type == Mux) {
			int a = count(d.mux_a);
			int b = count(d.mux_b);
			int c = count(d.mux_v);
			return a + b + c + 1;
		}
		else {
			return 1;
		}
	
}

bit check([int n], Dag p, int[n] assignment) {
	Dag r = run(p, assignment);
	return (r.type == Bool && r.x);
		
}

bit equals(Dag a, Dag b) {
	if (a == null){ return true;}
	if (b == null){return false;}
	
	
		if (a.type == Num && b.type == Num) {
			return a.val == b.val;
		}
		
		if (a.type == Bool && b.type == Bool) {
			return a.x == b.x;
		}
		else return true;
	
}