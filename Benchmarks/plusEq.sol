void optimize (AST original, int[4] assignment, ref AST _out)/*../plusEq.sk:17*/
{
  _out = null;
  AST _out_s269 = null;
  AST AST__s5_s11 = new Bool(x=1);
  AST AST__s14_s15 = new Var(id=1, type=0);
  AST AST__s26_s30 = new Num(val=20);
  AST a_s2 = new Mux(v=AST__s5_s11, a=AST__s14_s15, b=AST__s26_s30);
  AST AST__s38_s39 = new Var(id=2, type=0);
  AST AST__s47_s48 = new Var(id=3, type=0);
  AST b_s35 = new Plus(a=AST__s38_s39, b=AST__s47_s48);
  AST AST__s1 = new Gt(a=a_s2, b=b_s35);
  int[32] tmp = {3,1,0,0,8,0,24,8,16,16,16,16,4,9,16,4,24,20,24,24,16,0,24,24,16,18,10,17,24,16,8,24};
  bit _out_s271 = 0;
  check(4, AST__s1, tmp[0::4], _out_s271);
  assert (_out_s271); //Assert at ../plusEq.sk:101 (1869649887783780346)
  bit _out_s273 = 0;
  check(4, AST__s1, assignment, _out_s273);
  bit _has_out_ = 0;
  if(_out_s273)/*../plusEq.sk:103*/
  {
    AST AST__s105 = new Bool(x=0);
    int _out_s275 = 0;
    count(AST__s105, _out_s275);
    int _out_s277 = 0;
    count(original, _out_s277);
    assert (_out_s275 < _out_s277); //Assert at ../plusEq.sk:105 (-3025268751483019126)
    _out_s269 = AST__s105;
    _has_out_ = 1;
  }
  if(_has_out_ == 0)/*../plusEq.sk:108*/
  {
    _out_s269 = original;
  }
  _out = _out_s269;
  return;
}