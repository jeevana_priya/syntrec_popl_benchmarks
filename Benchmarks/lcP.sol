void desugar (srcAST s, ref dstAST _out)/*lcP.sk:34*/
{
  _out = null;
  ID id2 = new ID();
  dstAST ret_s1159 = null;
  bit _has_out_ = 0;
  if(s == (null))/*lcP.sk:6*/
  {
    ret_s1159 = null;
    _has_out_ = 1;
  }
  if(_has_out_ == 0)/*lcP.sk:7*/
  {
    switch(s){/*lcP.sk:7*/
      case NumS:
      {
        int tmp_s208 = s.val;
        dstAST dstAST__s4 = new NumD(val=tmp_s208);
        ret_s1159 = dstAST__s4;
      }
      case PairS:
      {
        srcAST s_29d = s.a;
        srcAST s_29d_0 = s.b;
        dstAST[2] a_s1207 = ((dstAST[2])null);
        dstAST _out_s1345 = null;
        desugar(s_29d, _out_s1345);
        a_s1207[0] = _out_s1345;
        dstAST _out_s1345_0 = null;
        desugar(s_29d_0, _out_s1345_0);
        a_s1207[1] = _out_s1345_0;
        VarD var_s217 = new VarD(name=id1);
        dstAST dstAST__s252_s253 = new VarD(name=id1);
        dstAST tmp_s264 = a_s1207[0];
        dstAST a_s236 = new AppD(a=dstAST__s252_s253, b=tmp_s264);
        dstAST tmp_s282 = a_s1207[1];
        dstAST a_s223 = new AppD(a=a_s236, b=tmp_s282);
        dstAST dstAST__s211 = new AbsD(var=var_s217, a=a_s223);
        ret_s1159 = dstAST__s211;
      }
      case FirstS:
      {
        srcAST s_40a = s.a;
        dstAST[1] a_s1253 = ((dstAST[1])null);
        dstAST _out_s1345_1 = null;
        desugar(s_40a, _out_s1345_1);
        a_s1253[0] = _out_s1345_1;
        dstAST tmp_s448 = a_s1253[0];
        VarD var_s555 = new VarD(name=id3);
        VarD VarD__s570 = new VarD(name=id2);
        dstAST dstAST__s577_s578 = new VarD(name=id3);
        dstAST a_s561 = new AbsD(var=VarD__s570, a=dstAST__s577_s578);
        dstAST b_s548 = new AbsD(var=var_s555, a=a_s561);
        dstAST dstAST__s433 = new AppD(a=tmp_s448, b=b_s548);
        ret_s1159 = dstAST__s433;
      }
      case SecondS:
      {
        srcAST s_575 = s.a;
        dstAST[1] a_s1299 = ((dstAST[1])null);
        dstAST _out_s1345_2 = null;
        desugar(s_575, _out_s1345_2);
        a_s1299[0] = _out_s1345_2;
        dstAST tmp_s670 = a_s1299[0];
        VarD var_s777 = new VarD(name=id2);
        VarD VarD__s792 = new VarD(name=id3);
        dstAST dstAST__s799_s800 = new VarD(name=id3);
        dstAST a_s783 = new AbsD(var=VarD__s792, a=dstAST__s799_s800);
        dstAST b_s770 = new AbsD(var=var_s777, a=a_s783);
        dstAST dstAST__s655 = new AppD(a=tmp_s670, b=b_s770);
        ret_s1159 = dstAST__s655;
      }
    }
  }
  _out = ret_s1159;
  return;
}
      