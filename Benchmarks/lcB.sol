void desugar (srcAST s, ref dstAST _out)/*lcB.sk:32*/
{
  _out = null;
  ID id1 = new ID();
  ID id2 = new ID();
  ID id3 = new ID();
  dstAST ret_s2026 = null;
  bit _has_out_ = 0;
  if(s == (null))/*lcB.sk:6*/
  {
    ret_s2026 = null;
    _has_out_ = 1;
  }
  if(_has_out_ == 0)/*lcB.sk:7*/
  {
    switch(s){/*lcB.sk:7*/
      case AndS:
      {
        srcAST s_20b = s.a;
        srcAST s_20b_0 = s.b;
        dstAST[2] a_s2028 = ((dstAST[2])null);
        dstAST _out_s2498 = null;
        desugar(s_20b, _out_s2498);
        a_s2028[0] = _out_s2498;
        dstAST _out_s2498_0 = null;
        desugar(s_20b_0, _out_s2498_0);
        a_s2028[1] = _out_s2498_0;
        dstAST tmp_s32 = a_s2028[0];
        dstAST tmp_s103 = a_s2028[1];
        dstAST a_s16 = new AppD(a=tmp_s32, b=tmp_s103);
        dstAST tmp_s174 = a_s2028[0];
        dstAST dstAST__s4 = new AppD(a=a_s16, b=tmp_s174);
        ret_s2026 = dstAST__s4;
      }
      case OrS:
      {
        srcAST s_46c = s.a;
        srcAST s_46c_0 = s.b;
        dstAST[2] a_s2122 = ((dstAST[2])null);
        dstAST _out_s2498_1 = null;
        desugar(s_46c, _out_s2498_1);
        a_s2122[0] = _out_s2498_1;
        dstAST _out_s2498_2 = null;
        desugar(s_46c_0, _out_s2498_2);
        a_s2122[1] = _out_s2498_2;
        dstAST tmp_s354 = a_s2122[0];
        dstAST tmp_s425 = a_s2122[0];
        dstAST a_s338 = new AppD(a=tmp_s354, b=tmp_s425);
        dstAST tmp_s496 = a_s2122[1];
        dstAST dstAST__s326 = new AppD(a=a_s338, b=tmp_s496);
        ret_s2026 = dstAST__s326;
      }
      case NotS:
      {
        srcAST s_6cd = s.a;
        dstAST[1] a_s2216 = ((dstAST[1])null);
        dstAST _out_s2498_3 = null;
        desugar(s_6cd, _out_s2498_3);
        a_s2216[0] = _out_s2498_3;
        dstAST tmp_s676 = a_s2216[0];
        VarD var_s751 = new VarD(name=id1);
        VarD VarD__s766 = new VarD(name=id2);
        dstAST dstAST__s773_s774 = new VarD(name=id2);
        dstAST a_s757 = new AbsD(var=VarD__s766, a=dstAST__s773_s774);
        dstAST b_s744 = new AbsD(var=var_s751, a=a_s757);
        dstAST a_s660 = new AppD(a=tmp_s676, b=b_s744);
        VarD var_s822 = new VarD(name=id3);
        VarD var_s835 = new VarD(name=id2);
        dstAST a_s841 = new VarD(name=id3);
        dstAST a_s828 = new AbsD(var=var_s835, a=a_s841);
        dstAST b_s815 = new AbsD(var=var_s822, a=a_s828);
        dstAST dstAST__s648 = new AppD(a=a_s660, b=b_s815);
        ret_s2026 = dstAST__s648;
      }
      case TrueS:
      {
        VarD var_s975 = new VarD(name=id1);
        ID name_s990 = new ID();
        VarD var_s987 = new VarD(name=name_s990);
        dstAST a_s993 = new VarD(name=id1);
        dstAST a_s981 = new AbsD(var=var_s987, a=a_s993);
        dstAST dstAST__s970 = new AbsD(var=var_s975, a=a_s981);
        ret_s2026 = dstAST__s970;
      }
      case FalseS:
      {
        VarD var_s1266 = new VarD(name=id1);
        VarD var_s1278 = new VarD(name=id2);
        dstAST a_s1284 = new VarD(name=id2);
        dstAST a_s1272 = new AbsD(var=var_s1278, a=a_s1284);
        dstAST dstAST__s1261 = new AbsD(var=var_s1266, a=a_s1272);
        ret_s2026 = dstAST__s1261;
      }
    }
  }
  _out = ret_s2026;
  return;
}
      
      
