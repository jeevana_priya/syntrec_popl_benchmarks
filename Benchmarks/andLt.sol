void optimize (AST original, int[4] assignment, ref AST _out)/*../andLt.sk:11*/
{
  _out = null;
  AST _out_s269 = null;
  AST AST__s5_s9 = new Num(val=8);
  AST AST__s14_s18 = new Num(val=8);
  AST a_s2 = new Equal(a=AST__s5_s9, b=AST__s14_s18);
  AST AST__s38_s39 = new Var(id=3, type=0);
  AST AST__s47_s48 = new Var(id=2, type=0);
  AST b_s35 = new Lt(a=AST__s38_s39, b=AST__s47_s48);
  AST AST__s1 = new And(a=a_s2, b=b_s35);
  int[32] tmp = {12,16,12,8,20,18,17,4,16,2,4,8,15,4,5,0,20,19,16,16,17,12,8,0,16,4,16,16,24,24,16,24};
  bit _out_s271 = 0;
  check(4, AST__s1, tmp[0::4], _out_s271);
  assert (_out_s271); //Assert at ../andLt.sk:101 (7099520183097595233)
  bit _out_s273 = 0;
  check(4, AST__s1, assignment, _out_s273);
  bit _has_out_ = 0;
  if(_out_s273)/*../andLt.sk:103*/
  {
    AST AST__s105 = new Bool(x=0);
    int _out_s275 = 0;
    count(AST__s105, _out_s275);
    int _out_s277 = 0;
    count(original, _out_s277);
    assert (_out_s275 < _out_s277); //Assert at ../andLt.sk:105 (8026986557734911684)
    _out_s269 = AST__s105;
    _has_out_ = 1;
  }
  if(_has_out_ == 0)/*../andLt.sk:108*/
  {
    _out_s269 = original;
  }
  _out = _out_s269;
  return;
}
