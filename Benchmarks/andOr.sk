//@Description This benchmark synthesizes optimizations on ASTs.
/**
 * Optimal bounds:
 * 		GUC depth: 3
 * 		Inlining limit: 3
 */
 
#include "lib.skh"
#include "astOptim.skh"

pragma options "--bnd-inline-amnt 3"; 

/**
 * Function to synthesize
 */
adt AST {
	Equal {AST a; AST b;}
	Plus {AST a; AST b;}
	Lt {AST a; AST b;}
	Gt {AST a; AST b;}
	Var {int id; bit type; }
	Num {int val;}
	And {AST a; AST b;}
	Or {AST a; AST b;}
	Not {AST a;}
	Mux {AST v; AST a; AST b;}
	Bool {bit x;}
}

bit Bit = 1; // To indicate whether a variable corresponds to Num or Bool
bit Int = 0;

/**
 * Sketch of the optimization function.
 * This function optimizes the given AST "original" by finding a predicate 
 * and an optimized AST if that predicate is satisfied.
 */
AST optimize (AST original, int[4] assignment) {
	return optimize_gen(original, assignment, check, count);
}

/*
 * Specification to the optimize the tree below
 * 
 *	a 	b  c   d
 *   \ /    \ /
 *    ||     ||
 *     \   /
 *       &&
 *
 * Here, the correctness is guaranteed for all possible assignments (bounded) for the variables a,b,c,d
 */
let original = new And(a = new Or(a= new Var(id = 0, type = Bit), b = new Var(id = 1, type = Bit)), b = new Or(a = new Var(id = 2, type = Bit), b= new Var(id =3, type = Bit))),
	optNode = optimize(original, assignment) {
	assert(equals(run(4, original, assignment), run(4, optNode, assignment)));
}

