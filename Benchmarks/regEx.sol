void desugar (srcExpr s, ref dstExpr _out)/*../regEx.sk:27*/
{
  _out = null;
  dstExpr ret_s770 = null;
  bit _has_out_ = 0;
  if(s == (null))/*../regEx.sk:6*/
  {
    ret_s770 = null;
    _has_out_ = 1;
  }
  if(_has_out_ == 0)/*../regEx.sk:7*/
  {
    switch(s){/*../regEx.sk:7*/
      case VarS:
      {
        int tmp_s20 = s.id;
        dstExpr dstExpr__s16 = new VarD(id=tmp_s20);
        ret_s770 = dstExpr__s16;
      }
      case AltS:
      {
        srcExpr s_17e = s.a;
        srcExpr s_17e_0 = s.b;
        dstExpr[2] a_s774 = ((dstExpr[2])null);
        dstExpr _out_s778 = null;
        desugar(s_17e, _out_s778);
        a_s774[0] = _out_s778;
        dstExpr _out_s778_0 = null;
        desugar(s_17e_0, _out_s778_0);
        a_s774[1] = _out_s778_0;
        dstExpr cond_s215 = new HoleD();
        dstExpr tmp_s282 = a_s774[0];
        dstExpr tmp_s346 = a_s774[1];
        dstExpr dstExpr__s207 = new IteD(cond=cond_s215, a=tmp_s282, b=tmp_s346);
        ret_s770 = dstExpr__s207;
      }
      case ChoiceS:
      {
        srcExpr s_255 = s.a;
        dstExpr[1] a_s776 = ((dstExpr[1])null);
        dstExpr _out_s778_1 = null;
        desugar(s_255, _out_s778_1);
        a_s776[0] = _out_s778_1;
        dstExpr cond_s419 = new BoolD(v=1);
        dstExpr dstExpr__s496_s502 = new HoleD();
        dstExpr tmp_s511 = a_s776[0];
        dstExpr dstExpr__s530_s537 = new EmptyD();
        dstExpr a_s483 = new IteD(cond=dstExpr__s496_s502, a=tmp_s511, b=dstExpr__s530_s537);
        dstExpr b_s547 = new EmptyD();
        dstExpr dstExpr__s411 = new IteD(cond=cond_s419, a=a_s483, b=b_s547);
        ret_s770 = dstExpr__s411;
      }
    }
  }
  _out = ret_s770;
  return;
}
      