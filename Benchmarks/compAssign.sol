void desugar (srcExpr e, ref dstExpr _out)/*compAssign.sk:40*/
{
  _out = null;
  dstExpr ret_s934 = null;
  bit _has_out_ = 0;
  if(e == (null))/*compAssign.sk:6*/
  {
    ret_s934 = null;
    _has_out_ = 1;
  }
  if(_has_out_ == 0)/*compAssign.sk:7*/
  {
    switch(e){/*compAssign.sk:7*/
      case AssignS:
      {
        srcExpr s_f2 = e.a;
        dstExpr[1] a_s936 = ((dstExpr[1])null);
        dstExpr _out_s948 = null;
        desugar(s_f2, _out_s948);
        a_s936[0] = _out_s948;
        int tmp_s30 = e.var;
        dstExpr tmp_s36 = a_s936[0];
        dstExpr dstExpr__s25 = new AssignD(var=tmp_s30, a=tmp_s36);
        ret_s934 = dstExpr__s25;
      }
      case IncrAssignS:
      {
        srcExpr s_174 = e.a;
        dstExpr[1] a_s938 = ((dstExpr[1])null);
        dstExpr _out_s948_0 = null;
        desugar(s_174, _out_s948_0);
        a_s938[0] = _out_s948_0;
        int tmp_s151 = e.var;
        int tmp_s170 = e.var;
        dstExpr dstExpr__s167_s168 = new VarD(id=tmp_s170);
        opcode opcode__s189 = new Oplus();
        dstExpr tmp_s192 = a_s938[0];
        dstExpr a_s154 = new BinaryD(op=opcode__s189, a=dstExpr__s167_s168, b=tmp_s192);
        dstExpr dstExpr__s146 = new AssignD(var=tmp_s151, a=a_s154);
        ret_s934 = dstExpr__s146;
      }
      case DecrAssignS:
      {
        srcExpr s_1f6 = e.a;
        dstExpr[1] a_s940 = ((dstExpr[1])null);
        dstExpr _out_s948_1 = null;
        desugar(s_1f6, _out_s948_1);
        a_s940[0] = _out_s948_1;
        int tmp_s272 = e.var;
        int tmp_s291 = e.var;
        dstExpr dstExpr__s288_s289 = new VarD(id=tmp_s291);
        opcode opcode__s310 = new Ominus();
        dstExpr tmp_s313 = a_s940[0];
        dstExpr a_s275 = new BinaryD(op=opcode__s310, a=dstExpr__s288_s289, b=tmp_s313);
        dstExpr dstExpr__s267 = new AssignD(var=tmp_s272, a=a_s275);
        ret_s934 = dstExpr__s267;
      }
      case VarS:
      {
        int tmp_s444 = e.id;
        dstExpr dstExpr__s388 = new VarD(id=tmp_s444);
        ret_s934 = dstExpr__s388;
      }
      case NumS:
      {
        int tmp_s562 = e.val;
        dstExpr dstExpr__s502 = new NumD(val=tmp_s562);
        ret_s934 = dstExpr__s502;
      }
      case BinaryS:
      {
        srcExpr s_36a = e.a;
        srcExpr s_36a_0 = e.b;
        dstExpr[2] a_s946 = ((dstExpr[2])null);
        dstExpr _out_s948_2 = null;
        desugar(s_36a, _out_s948_2);
        a_s946[0] = _out_s948_2;
        dstExpr _out_s948_3 = null;
        desugar(s_36a_0, _out_s948_3);
        a_s946[1] = _out_s948_3;
        dstExpr tmp_s627 = a_s946[0];
        opcode tmp_s684 = e.op;
        dstExpr tmp_s689 = a_s946[1];
        dstExpr dstExpr__s616 = new BinaryD(op=tmp_s684, a=tmp_s627, b=tmp_s689);
        ret_s934 = dstExpr__s616;
      }
    }
  }
  _out = ret_s934;
  return;
}
      
