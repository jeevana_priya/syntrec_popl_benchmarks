void desugar (srcAST s, ref dstAST _out)/*../elimBool.sk:26*/
{
  _out = null;
  dstAST ret_s462 = null;
  bit _has_out_ = 0;
  if(s == (null))/*../elimBool.sk:6*/
  {
    ret_s462 = null;
    _has_out_ = 1;
  }
  if(_has_out_ == 0)/*../elimBool.sk:7*/
  {
    switch(s){/*../elimBool.sk:7*/
      case AndS:
      {
        srcAST s_69 = s.a;
        srcAST s_69_0 = s.b;
        dstAST[2] a_s464 = ((dstAST[2])null);
        dstAST _out_s470 = null;
        desugar(s_69, _out_s470);
        a_s464[0] = _out_s470;
        dstAST _out_s470_0 = null;
        desugar(s_69_0, _out_s470_0);
        a_s464[1] = _out_s470_0;
        dstAST tmp_s22 = a_s464[1];
        dstAST tmp_s32 = a_s464[0];
        dstAST tmp_s42 = a_s464[1];
        dstAST a_s15 = new IfD(a=tmp_s22, b=tmp_s32, c=tmp_s42);
        dstAST tmp_s57 = a_s464[1];
        dstAST tmp_s95 = null;
        dstAST dstAST__s11 = new IfD(a=a_s15, b=tmp_s57, c=tmp_s95);
        ret_s462 = dstAST__s11;
      }
      case OrS:
      {
        srcAST s_f1 = s.a;
        srcAST s_f1_0 = s.b;
        dstAST[2] a_s466 = ((dstAST[2])null);
        dstAST _out_s470_1 = null;
        desugar(s_f1, _out_s470_1);
        a_s466[0] = _out_s470_1;
        dstAST _out_s470_2 = null;
        desugar(s_f1_0, _out_s470_2);
        a_s466[1] = _out_s470_2;
        dstAST tmp_s143 = a_s466[0];
        dstAST tmp_s182 = a_s466[0];
        dstAST tmp_s221 = a_s466[1];
        dstAST dstAST__s136 = new IfD(a=tmp_s143, b=tmp_s182, c=tmp_s221);
        ret_s462 = dstAST__s136;
      }
      case BoolS:
      {
        bit tmp_s370 = s.v;
        dstAST dstAST__s261 = new BoolD(v=tmp_s370);
        ret_s462 = dstAST__s261;
      }
    }
  }
  _out = ret_s462;
  return;
}
      
