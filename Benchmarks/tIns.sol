void insertNode (BinaryTree tree, int x, ref BinaryTree _out)/*../tIns.sk:27*/
{
  _out = null;
  BinaryTree _out_s329 = null;
  bit _has_out_ = 0;
  if(tree == (null))/*../tIns.sk:6*/
  {
    _out_s329 = null;
    _has_out_ = 1;
  }
  if(_has_out_ == 0)/*../tIns.sk:7*/
  {
    switch(tree){/*../tIns.sk:7*/
      case Branch:
      {
        BinaryTree s_23 = tree.l;
        BinaryTree s_23_0 = tree.r;
        BinaryTree[2] a_s331 = ((BinaryTree[2])null);
        BinaryTree _out_s405 = null;
        insertNode(s_23, x, _out_s405);
        a_s331[0] = _out_s405;
        BinaryTree _out_s405_0 = null;
        insertNode(s_23_0, x, _out_s405_0);
        a_s331[1] = _out_s405_0;
        bit _out_s333 = x > (tree.value);
        if(_out_s333)/*../tIns.sk:13*/
        {
          int tmp_s20 = tree.value;
          BinaryTree tmp_s25 = tree.l;
          BinaryTree tmp_s61 = a_s331[1];
          BinaryTree BinaryTree__s15 = new Branch(value=tmp_s20, l=tmp_s25, r=tmp_s61);
          _out_s329 = BinaryTree__s15;
          _has_out_ = 1;
        }
        bit _out_s333_0 = 0;
        if(_has_out_ == 0)/*../tIns.sk:13*/
        {
          _out_s333_0 = x <= (tree.value);
        }
        if(_has_out_ == 0)/*../tIns.sk:13*/
        {
          if(_out_s333_0)/*../tIns.sk:13*/
          {
            int tmp_s20_0 = tree.value;
            BinaryTree tmp_s26 = a_s331[0];
            BinaryTree tmp_s60 = tree.r;
            BinaryTree BinaryTree__s15_0 = new Branch(value=tmp_s20_0, l=tmp_s26, r=tmp_s60);
            _out_s329 = BinaryTree__s15_0;
          }
        }
      }
      case Leaf:
      {
        bit _out_s357 = x <= (tree.value);
        if(_out_s357)/*../tIns.sk:13*/
        {
          BinaryTree l_s104 = new Empty();
          int tmp_s165 = tree.value;
          BinaryTree r_s136 = new Leaf(value=tmp_s165);
          BinaryTree BinaryTree__s97 = new Branch(value=x, l=l_s104, r=r_s136);
          _out_s329 = BinaryTree__s97;
          _has_out_ = 1;
        }
        bit _out_s357_0 = 0;
        if(_has_out_ == 0)/*../tIns.sk:13*/
        {
          _out_s357_0 = x > (tree.value);
        }
        if(_has_out_ == 0)/*../tIns.sk:13*/
        {
          if(_out_s357_0)/*../tIns.sk:13*/
          {
            int tmp_s133 = tree.value;
            BinaryTree l_s104_0 = new Leaf(value=tmp_s133);
            BinaryTree r_s136_0 = new Empty();
            BinaryTree BinaryTree__s97_0 = new Branch(value=x, l=l_s104_0, r=r_s136_0);
            _out_s329 = BinaryTree__s97_0;
          }
        }
      }
      case Empty:
      {
        if(x >= 0)/*../tIns.sk:13*/
        {
          BinaryTree BinaryTree__s172 = new Leaf(value=x);
          _out_s329 = BinaryTree__s172;
        }
      }
    }
  }
  _out = _out_s329;
  return;
}
        
