void desugar (srcAST s, ref dstAST _out)/*../mergeOp.sk:58*/
{
  _out = null;
  dstAST ret_s1672 = null;
  bit _has_out_ = 0;
  if(s == (null))/*../mergeOp.sk:6*/
  {
    ret_s1672 = null;
    _has_out_ = 1;
  }
  if(_has_out_ == 0)/*../mergeOp.sk:7*/
  {
    switch(s){/*../mergeOp.sk:7*/
      case NumS:
      {
        opcode op_s47 = new Oneg();
        opcode opcode__s65 = new Oneg();
        int tmp_s72 = s.val;
        dstAST dstAST__s69_s70 = new NumD(val=tmp_s72);
        dstAST a_s50 = new UnaryD(op=opcode__s65, a=dstAST__s69_s70);
        dstAST dstAST__s36 = new UnaryD(op=op_s47, a=a_s50);
        ret_s1672 = dstAST__s36;
      }
      case TrueS:
      {
        dstAST dstAST__s138 = new BoolD(v=1);
        ret_s1672 = dstAST__s138;
      }
      case FalseS:
      {
        dstAST dstAST__s240 = new BoolD(v=0);
        ret_s1672 = dstAST__s240;
      }
      case PlusS:
      {
        srcAST s_2dd = s.a;
        srcAST s_2dd_0 = s.b;
        dstAST[2] a_s1680 = ((dstAST[2])null);
        dstAST _out_s1698 = null;
        desugar(s_2dd, _out_s1698);
        a_s1680[0] = _out_s1698;
        dstAST _out_s1698_0 = null;
        desugar(s_2dd_0, _out_s1698_0);
        a_s1680[1] = _out_s1698_0;
        opcode op_s354 = new Oplus();
        dstAST tmp_s360 = a_s1680[0];
        dstAST tmp_s407 = a_s1680[1];
        dstAST dstAST__s342 = new BinaryD(op=op_s354, a=tmp_s360, b=tmp_s407);
        ret_s1672 = dstAST__s342;
      }
      case MinusS:
      {
        srcAST s_355 = s.a;
        srcAST s_355_0 = s.b;
        dstAST[2] a_s1682 = ((dstAST[2])null);
        dstAST _out_s1698_1 = null;
        desugar(s_355, _out_s1698_1);
        a_s1682[0] = _out_s1698_1;
        dstAST _out_s1698_2 = null;
        desugar(s_355_0, _out_s1698_2);
        a_s1682[1] = _out_s1698_2;
        opcode op_s463 = new Oplus();
        opcode opcode__s482 = new Oneg();
        dstAST tmp_s485 = a_s1682[1];
        dstAST a_s466 = new UnaryD(op=opcode__s482, a=tmp_s485);
        opcode opcode__s529 = new Oplus();
        dstAST tmp_s532 = a_s1682[0];
        dstAST dstAST__s549_s550 = new NumD(val=0);
        dstAST b_s513 = new BinaryD(op=opcode__s529, a=tmp_s532, b=dstAST__s549_s550);
        dstAST dstAST__s451 = new BinaryD(op=op_s463, a=a_s466, b=b_s513);
        ret_s1672 = dstAST__s451;
      }
      case NegS:
      {
        srcAST s_3cd = s.a;
        dstAST[1] a_s1684 = ((dstAST[1])null);
        dstAST _out_s1698_3 = null;
        desugar(s_3cd, _out_s1698_3);
        a_s1684[0] = _out_s1698_3;
        opcode op_s572 = new Ominus();
        opcode opcode__s591 = new Ominus();
        dstAST tmp_s594 = a_s1684[0];
        dstAST tmp_s609 = a_s1684[0];
        dstAST a_s575 = new BinaryD(op=opcode__s591, a=tmp_s594, b=tmp_s609);
        dstAST tmp_s625 = a_s1684[0];
        dstAST dstAST__s560 = new BinaryD(op=op_s572, a=a_s575, b=tmp_s625);
        ret_s1672 = dstAST__s560;
      }
      case AndS:
      {
        srcAST s_443 = s.a;
        srcAST s_443_0 = s.b;
        dstAST[2] a_s1686 = ((dstAST[2])null);
        dstAST _out_s1698_4 = null;
        desugar(s_443, _out_s1698_4);
        a_s1686[0] = _out_s1698_4;
        dstAST _out_s1698_5 = null;
        desugar(s_443_0, _out_s1698_5);
        a_s1686[1] = _out_s1698_5;
        opcode op_s681 = new Oand();
        dstAST tmp_s687 = a_s1686[1];
        dstAST tmp_s734 = a_s1686[0];
        dstAST dstAST__s669 = new BinaryD(op=op_s681, a=tmp_s687, b=tmp_s734);
        ret_s1672 = dstAST__s669;
      }
      case OrS:
      {
        srcAST s_4bb = s.a;
        srcAST s_4bb_0 = s.b;
        dstAST[2] a_s1688 = ((dstAST[2])null);
        dstAST _out_s1698_6 = null;
        desugar(s_4bb, _out_s1698_6);
        a_s1688[0] = _out_s1698_6;
        dstAST _out_s1698_7 = null;
        desugar(s_4bb_0, _out_s1698_7);
        a_s1688[1] = _out_s1698_7;
        opcode op_s790 = new Oor();
        dstAST tmp_s796 = a_s1688[0];
        dstAST tmp_s843 = a_s1688[1];
        dstAST dstAST__s778 = new BinaryD(op=op_s790, a=tmp_s796, b=tmp_s843);
        ret_s1672 = dstAST__s778;
      }
      case NotS:
      {
        srcAST s_533 = s.a;
        dstAST[1] a_s1690 = ((dstAST[1])null);
        dstAST _out_s1698_8 = null;
        desugar(s_533, _out_s1698_8);
        a_s1690[0] = _out_s1698_8;
        opcode op_s899 = new Oand();
        opcode opcode__s918 = new Onot();
        dstAST tmp_s921 = a_s1690[0];
        dstAST a_s902 = new UnaryD(op=opcode__s918, a=tmp_s921);
        opcode opcode__s965 = new Ogt();
        dstAST dstAST__s970_s971 = new NumD(val=24);
        dstAST dstAST__s985_s986 = new NumD(val=1);
        dstAST b_s949 = new BinaryD(op=opcode__s965, a=dstAST__s970_s971, b=dstAST__s985_s986);
        dstAST dstAST__s887 = new BinaryD(op=op_s899, a=a_s902, b=b_s949);
        ret_s1672 = dstAST__s887;
      }
      case LtS:
      {
        srcAST s_5a9 = s.a;
        srcAST s_5a9_0 = s.b;
        dstAST[2] a_s1692 = ((dstAST[2])null);
        dstAST _out_s1698_9 = null;
        desugar(s_5a9, _out_s1698_9);
        a_s1692[0] = _out_s1698_9;
        dstAST _out_s1698_10 = null;
        desugar(s_5a9_0, _out_s1698_10);
        a_s1692[1] = _out_s1698_10;
        opcode op_s1008 = new Ogt();
        opcode opcode__s1027 = new Oplus();
        dstAST tmp_s1030 = a_s1692[1];
        dstAST dstAST__s1047_s1048 = new NumD(val=0);
        dstAST a_s1011 = new BinaryD(op=opcode__s1027, a=tmp_s1030, b=dstAST__s1047_s1048);
        dstAST tmp_s1061 = a_s1692[0];
        dstAST dstAST__s996 = new BinaryD(op=op_s1008, a=a_s1011, b=tmp_s1061);
        ret_s1672 = dstAST__s996;
      }
      case GtS:
      {
        srcAST s_621 = s.a;
        srcAST s_621_0 = s.b;
        dstAST[2] a_s1694 = ((dstAST[2])null);
        dstAST _out_s1698_11 = null;
        desugar(s_621, _out_s1698_11);
        a_s1694[0] = _out_s1698_11;
        dstAST _out_s1698_12 = null;
        desugar(s_621_0, _out_s1698_12);
        a_s1694[1] = _out_s1698_12;
        opcode op_s1117 = new Ogt();
        opcode opcode__s1136 = new Oplus();
        dstAST dstAST__s1141_s1142 = new NumD(val=0);
        dstAST tmp_s1154 = a_s1694[0];
        dstAST a_s1120 = new BinaryD(op=opcode__s1136, a=dstAST__s1141_s1142, b=tmp_s1154);
        dstAST tmp_s1170 = a_s1694[1];
        dstAST dstAST__s1105 = new BinaryD(op=op_s1117, a=a_s1120, b=tmp_s1170);
        ret_s1672 = dstAST__s1105;
      }
      case EqS:
      {
        srcAST s_699 = s.a;
        srcAST s_699_0 = s.b;
        dstAST[2] a_s1696 = ((dstAST[2])null);
        dstAST _out_s1698_13 = null;
        desugar(s_699, _out_s1698_13);
        a_s1696[0] = _out_s1698_13;
        dstAST _out_s1698_14 = null;
        desugar(s_699_0, _out_s1698_14);
        a_s1696[1] = _out_s1698_14;
        opcode op_s1226 = new Oeq();
        dstAST tmp_s1232 = a_s1696[1];
        dstAST tmp_s1279 = a_s1696[0];
        dstAST dstAST__s1214 = new BinaryD(op=op_s1226, a=tmp_s1232, b=tmp_s1279);
        ret_s1672 = dstAST__s1214;
      }
    }
  }
  _out = ret_s1672;
  return;
}
      
      
      
