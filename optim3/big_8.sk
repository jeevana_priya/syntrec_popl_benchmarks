pragma options "--bnd-inline-amnt 4 --bnd-inbits 4 --bnd-src-tuple-depth 3";
#include "../lib.skh"
adt srcAST {
	NumS {int val; }
	TrueS {}
	FalseS {}
	PlusS {srcAST a; srcAST b; }
	MinusS {srcAST a; srcAST b; }
	NegS {srcAST a; }
	AndS {srcAST a; srcAST b;}
	OrS {srcAST a; srcAST b;}
	//NotS {srcAST a;}
	//LtS {srcAST a; srcAST b;}
	//GtS {srcAST a; srcAST b;}
	//EqS {srcAST a; srcAST b;}	
}

adt dstAST {
	NumD {int val;}
	BoolD {bit v;}
	UnaryD {opcode op; dstAST a;}
	BinaryD {opcode op; dstAST a; dstAST b;}
}

adt baseAST {
	Num {int val;}
	Bool {bit v; }	
}

adt opcode{
	Oplus{}
	Ominus{}
	Oneg{}
	Oand{}
	Oor{}
	Onot{}
	Ogt{}
	Olt{}
	Oeq {}	
}


dstAST desugar(srcAST s){
	return recursiveReplacer(s, desugar);
}

let a = interpretSrcAST(s) {
	precond (a != null)
	assert(a === interpretDstAST(desugar(s)));	
	
}

baseAST interpretSrcAST(srcAST s){
	if(s==null) return null;
	
	switch(s){
		case PlusS: {
			baseAST sa = interpretSrcAST(s.a);
			if (sa == null) return null;
			baseAST sb = interpretSrcAST(s.b);
			if (sb == null) return null;
			int a, b;
			switch(sa) {
				case Num: a = sa.val;
				default: return null;	
			}
			switch(sb) {
				case Num: b = sb.val;
				default: return null;	
			}
			return new Num(val = a + b);
		}
		case MinusS: {
			baseAST sa = interpretSrcAST(s.a);
			if (sa == null) return null;
			baseAST sb = interpretSrcAST(s.b);
			if (sb == null) return null;
			int a, b;
			switch(sa) {
				case Num: a = sa.val;
				default: return null;	
			}
			switch(sb) {
				case Num: b = sb.val;
				default: return null;	
			}
			return new Num(val = a - b);
		}
		case AndS: {
			baseAST sa = interpretSrcAST(s.a);
			if (sa == null) return null;
			baseAST sb = interpretSrcAST(s.b);
			if (sb == null) return null;
			bit a, b;
			switch(sa) {
				case Bool: a = sa.v;
				default: return null;	
			}
			switch(sb) {
				case Bool: b = sb.v;
				default: return null;	
			}
			return new Bool(v = a && b);
		}
		case OrS: {
			baseAST sa = interpretSrcAST(s.a);
			if (sa == null) return null;
			baseAST sb = interpretSrcAST(s.b);
			if (sb == null) return null;
			bit a, b;
			switch(sa) {
				case Bool: a = sa.v;
				default: return null;	
			}
			switch(sb) {
				case Bool: b = sb.v;
				default: return null;	
			}
			return new Bool(v = a || b);
		}
		/*case LtS: {
			baseAST sa = interpretSrcAST(s.a);
			if (sa == null) return null;
			baseAST sb = interpretSrcAST(s.b);
			if (sb == null) return null;
			int a, b;
			switch(sa) {
				case Num: a = sa.val;
				default: return null;	
			}
			switch(sb) {
				case Num: b = sb.val;
				default: return null;	
			}
			return new Bool(v = a < b);
		}*/
		/*case GtS: {
			baseAST sa = interpretSrcAST(s.a);
			if (sa == null) return null;
			baseAST sb = interpretSrcAST(s.b);
			if (sb == null) return null;
			int a, b;
			switch(sa) {
				case Num: a = sa.val;
				default: return null;	
			}
			switch(sb) {
				case Num: b = sb.val;
				default: return null;	
			}
			return new Bool(v = a > b);
		}*/
		/*case EqS: {
			baseAST sa = interpretSrcAST(s.a);
			if (sa == null) return null;
			baseAST sb = interpretSrcAST(s.b);
			if (sb == null) return null;
			int a, b;
			switch(sa) {
				case Num: a = sa.val;
				default: return null;	
			}
			switch(sb) {
				case Num: b = sb.val;
				default: return null;	
			}
			return new Bool(v = a == b);
		}*/
		case NegS: {
			baseAST sa = interpretSrcAST(s.a);
			if (sa == null) return null;
			
			switch(sa) {
				case Num: return new Num(val = -sa.val);
				default: return null;	
			}
		}
		/*case NotS: {
			baseAST sa = interpretSrcAST(s.a);
			if (sa == null) return null;
			
			switch(sa) {
				case Bool: return new Bool(v = !sa.v);
				default: return null;	
			}
		}*/
		
		case NumS: return new Num(val = s.val);
		case TrueS: return new Bool(v = 1);
		case FalseS: return new Bool(v = 0);
		default: return null;
	}
}


baseAST interpretDstAST(dstAST s){
    if(s==null) return null;
	switch(s){
		case UnaryD:
		{
			baseAST s_a = interpretDstAST(s.a);
			if(s_a==null) return null;
			
			opcode op = s.op;
			switch(op){
			case Oneg: {
				switch(s_a){
					case Num: return new Num(val = (0-s_a.val));
					default: return null;
				}
			}
			case Onot: {
				switch(s_a){
					case Bool: return new Bool( v = (!s_a.v));
					default: return null;
				}	
			}
			default:  return null;
			}
		}
		
		case BinaryD:
		{
			baseAST s_a = interpretDstAST(s.a);
			if(s_a==null) return null;
			
			baseAST s_b = interpretDstAST(s.b);
			if(s_b==null) return null;	
				
			opcode op = s.op;
			switch(op){
			case Oplus: {
				int a, b;
				switch(s_a){
					case Num: a = s_a.val;
					default: return null;
				}
				switch(s_b){
					case Num: b = s_b.val;
					default: return null;
				}
				return new Num(val = a+b);
			}
			case Ominus: {
				int a, b;
				switch(s_a){
					case Num: a = s_a.val;
					default: return null;
				}
				switch(s_b){
					case Num: b = s_b.val;
					default: return null;
				}
				return new Num(val = a-b);
			}
			case Oand: {
				bit a, b;
				switch(s_a){
					case Bool: a = s_a.v;
					default: return null;
				}
				switch(s_b){
					case Bool: b = s_b.v;
					default: return null;
				}
				return new Bool(v = a&&b);
			}
			case Oor: {
				bit a, b;
				switch(s_a){
					case Bool: a = s_a.v;
					default: return null;
				}
				switch(s_b){
					case Bool: b = s_b.v;
					default: return null;
				}
				return new Bool(v = a||b);
			}
			case Ogt: {
				int a, b;
				switch(s_a){
					case Num: a = s_a.val;
					default: return null;
				}
				switch(s_b){
					case Num: b = s_b.val;
					default: return null;
				}
				return new Bool(v = a>b);
			}
			case Olt: {
				int a, b;
				switch(s_a){
					case Num: a = s_a.val;
					default: return null;
				}
				switch(s_b){
					case Num: b = s_b.val;
					default: return null;
				}
				return new Bool(v = a<b);
			}
			case Oeq: {
				int a, b;
				switch(s_a){
					case Num: a = s_a.val;
					default: return null;
				}
				switch(s_b){
					case Num: b = s_b.val;
					default: return null;
				}
				return new Bool(v = a==b);
			}
			default: return null;	
			}				
		}
		
		case NumD: return new Num(val = s.val);
		case BoolD: return new Bool(v = s.v);
	}
}
