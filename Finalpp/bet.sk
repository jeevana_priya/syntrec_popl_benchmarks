//@Description This benchmark derives the desugar function between two simple toy languages using full flegded interpreters for asserting correctness.

/**
 * Bounds used:
 * 		Inlining limit: 3
 * 		GUC depth: 3
 *	    ADT size in specification: 2 
 */
pragma options "--bnd-inline-amnt 3"; 
#include "lib.skh"

/**
 * Data definitions
 */
adt srcAST {
	NumS {int val;}
	TrueS {}
	FalseS {}
	Prim1S {opcode op; srcAST a;}
	Prim2S {opcode op; srcAST a; srcAST b;}
	BetweenS {srcAST a;srcAST b;srcAST c;}
	IfS {srcAST a; srcAST b; srcAST c;}
}

adt dstAST {
	NumD {int val;}
	BoolD {bit v;}
	Prim1D {opcode op; dstAST a;}
	Prim2D {opcode op; dstAST a; dstAST b;}
	IfD {dstAST a; dstAST b; dstAST c;}
}

adt opcode{
	Oplus{}
	Ominus{}
	Oneg{}
	Oand{}
	Oor{}
	Onot{}
	Ogt{}
	Olt{}	
}

/* This is the base type that both the languages get interpreted to.*/
adt baseAST {
	Num {int val;}
	Bool {bit v; }	
}

/**
 * Template of the function to synthesize
 */
dstAST desugar(srcAST s){
	return recursiveReplacer(s, desugar);
}



/**
 * Specification
 */
let a = interpretSrcAST(s) {
	assume (a != null)
	assert(a === interpretDstAST(desugar(s)));	

}

/////////////////////////////////////////
// Helper functions used in the harness
/////////////////////////////////////////

/**
 * Interpreter for src language is used by the test harnesses to check the equivalence of the source and destination ASTs 
 */
baseAST interpretSrcAST(srcAST s){
	if(s==null) return null;

	switch(s){
	case Prim1S:
	{
		baseAST s_a = interpretSrcAST(s.a);
		if(s_a==null) return null;
		opcode op = s.op;
		if (op == null) return null;
		switch(op){
		case Oneg: {				
			switch(s_a){
			case Num: return new Num(val = (0-s_a.val));
			default: return null;
			}
		}
		case Onot: {
			switch(s_a){
			case Bool: return new Bool(v = !s_a.v);
			default: return null;
			}	
		}
		default: return null;
		}
	}

	case Prim2S:
	{
		baseAST s_a = interpretSrcAST(s.a);
		if (s_a == null) return null;
		baseAST s_b = interpretSrcAST(s.b);
		if (s_b == null) return null;
		opcode op  = s.op;
		if (op == null) return null;
		switch(op){
		case Oplus: {
			int a, b;
			switch(s_a){
			case Num: a = s_a.val;
			default: return null;
			}
			switch(s_b){
			case Num: b = s_b.val;
			default: return null;
			}
			return new Num(val = a+b);
		}
		case Ominus: {
			int a, b;
			switch(s_a){
			case Num: a = s_a.val;
			default: return null;
			}
			switch(s_b){
			case Num: b = s_b.val;
			default: return null;
			}
			return new Num(val = a-b);
		}
		case Oand: {
			bit a, b;
			switch(s_a){
			case Bool: a = s_a.v;
			default: return null;
			}
			switch(s_b){
			case Bool: b = s_b.v;
			default: return null;
			}
			return new Bool(v = a && b);

		}
		case Oor: {
			bit a, b;
			switch(s_a){
			case Bool: a = s_a.v;
			default: return null;
			}
			switch(s_b){
			case Bool: b = s_b.v;
			default: return null;
			}
			return new Bool(v = a || b);

		}
		case Ogt: {
			int a, b;
			switch(s_a){
			case Num: a = s_a.val;
			default: return null;
			}
			switch(s_b){
			case Num: b = s_b.val;
			default: return null;
			}
			return new Bool(v = a > b);

		}
		case Olt: {
			int a, b;
			switch(s_a){
			case Num: a = s_a.val;
			default: return null;
			}
			switch(s_b){
			case Num: b = s_b.val;
			default: return null;
			}
			return new Bool(v = a < b);

		}
		default: return null;	
		}			
	}
	case BetweenS:
	{
		int a, b, c;
		baseAST s_a = interpretSrcAST(s.a);

		if(s_a==null) return null;
		switch(s_a){
		case Num: a = s_a.val;
		default: return null;
		}
		baseAST s_b = interpretSrcAST(s.b);
		if(s_b==null) return null;
		switch(s_b){
		case Num: b = s_b.val;
		default: return null;
		}
		baseAST s_c = interpretSrcAST(s.c);
		if(s_c==null) return null;
		switch(s_c){
		case Num: c = s_c.val;
		default: return null;
		}
		return new Bool(v = a < b && b < c);

	}	
	case IfS:
	{
		baseAST c  = interpretSrcAST(s.a);
		if(c==null) return null;
		switch(c){
		case Bool: if (c.v) return interpretSrcAST(s.b);
		else return interpretSrcAST(s.c);
		default: return null;
		}
	}
	case NumS: return new Num(val = s.val);
	case TrueS: return new Bool(v = 1);
	case FalseS: return new Bool(v = 0);
	default: return null;
	}
}


/**
 * Interpreter for the dst language 
 */
baseAST interpretDstAST(dstAST s){
	if(s==null) return null;
	switch(s){
	case Prim1D:
	{
		baseAST s_a = interpretDstAST(s.a);
		opcode op = s.op;
		switch(op){
		case Oneg: {
			if(s_a==null) return null;
			switch(s_a){
			case Num: return new Num(val = (0-s_a.val));
			default: return null;
			}
		}
		case Onot: {
			if(s_a==null) return null;
			switch(s_a){
			case Bool: return new Bool( v = (!s_a.v));
			default: return null;
			}	
		}
		default:  return null;
		}
	}

	case Prim2D:
	{
		baseAST s_a = interpretDstAST(s.a);
		if(s_a==null) return null;
		baseAST s_b = interpretDstAST(s.b);
		if(s_b==null) return null;

		opcode op = s.op;
		switch(op){
		case Oplus: {
			int a, b;

			switch(s_a){
			case Num: a = s_a.val;
			default: return null;
			}
			switch(s_b){
			case Num: b = s_b.val;
			default: return null;
			}
			return new Num(val = a+b);
		}
		case Ominus: {
			int a, b;
			switch(s_a){
			case Num: a = s_a.val;
			default: return null;
			}
			switch(s_b){
			case Num: b = s_b.val;
			default: return null;
			}
			return new Num(val = a-b);
		}
		case Oand: {
			bit a, b;
			switch(s_a){
			case Bool: a = s_a.v;
			default: return null;
			}
			switch(s_b){
			case Bool: b = s_b.v;
			default: return null;
			}
			return new Bool(v = a&&b);
		}
		case Oor: {
			bit a, b;
			switch(s_a){
			case Bool: a = s_a.v;
			default: return null;
			}
			switch(s_b){
			case Bool: b = s_b.v;
			default: return null;
			}
			return new Bool(v = a||b);
		}
		case Ogt: {
			int a, b;
			switch(s_a){
			case Num: a = s_a.val;
			default: return null;
			}
			switch(s_b){
			case Num: b = s_b.val;
			default: return null;
			}
			return new Bool(v = a>b);
		}
		case Olt: {
			int a, b;
			switch(s_a){
			case Num: a = s_a.val;
			default: return null;
			}
			switch(s_b){
			case Num: b = s_b.val;
			default: return null;
			}
			return new Bool(v = a<b);
		}
		default: return null;	
		}				
	}
	case IfD:
	{
		baseAST c  = interpretDstAST(s.a);
		if(c==null) return null;
		switch(c){
		case Bool:
		{
			if (c.v ==1) return interpretDstAST(s.b);
			else return interpretDstAST(s.c);
		}
		default: return null;	
		}
	}
	case NumD: return new Num(val = s.val);
	case BoolD: return new Bool(v = s.v);
	}
}
