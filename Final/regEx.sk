//@Description This benchmark synthesizes the desugarings for regular expressions
/**
 * Bounds used:
 * 		GUC depth: 3
 */
 
#include "lib.skh"

/*
 * Data definitions
 */
adt srcExpr {
	VarS{int id;}
	AltS{srcExpr a; srcExpr b;} // (a | b)
	ChoiceS{srcExpr a;} // a?
}

adt dstExpr {
	VarD{int id;}
	HoleD { } /*Unknown boolean*/
	IteD {dstExpr cond; dstExpr a; dstExpr b;}	
	EmptyD {}
	BoolD {bit v;}
}

/*
 * Template of the function to be synthesized.
 */
dstExpr desugar(srcExpr s) {
	return recursiveReplacer(s, desugar);
}

/*
 * Specification
 */
assert(interpretSrc(s) == interpretDst(desugar(s)));


///////////////////////////////////////////////////////
// Helper functions used in the specification
////////////////////////////////////////////////////////

int MAX_VAR = 8; // Max number of variables that a regex can correspond to
int ILL_FORMED = -20; // Error code to denote ill formed expressions

/*
 * Interpreter for the source language.
 * The output here is a list of ids of variables that the regex produces.
 */
int[MAX_VAR] interpretSrc(srcExpr s) {
	if (s == null) return ILL_FORMED;	
	switch(s) {
		case VarS: {
			if (s.id <= 0) return ILL_FORMED;
			return {s.id};
		}
		case AltS: {
			int[MAX_VAR] new_set;
			int ct = 0;
			int[MAX_VAR] a_set = interpretSrc(s.a);
			int[MAX_VAR] b_set = interpretSrc(s.b);
			if (a_set == ILL_FORMED || b_set == ILL_FORMED) return ILL_FORMED;
			
			for (int i = 0; i < MAX_VAR; i++) {
				if (a_set[i] != 0) {
					new_set[ct++] = a_set[i];
				} 
			} 
			for (int i = 0; i < MAX_VAR; i++) {
				if (b_set[i] != 0) {
					new_set[ct++] = b_set[i];
				} 
			}
			return new_set; 
		}
		
		case ChoiceS: {
			int[MAX_VAR] new_set;
			int ct = 0;
			int[MAX_VAR] a_set = interpretSrc(s.a);
			if (a_set == ILL_FORMED) return ILL_FORMED;
			for (int i = 0; i < MAX_VAR; i++) {
				if (a_set[i] != 0) {
					new_set[ct++] = a_set[i];
				} 
			} 
			new_set[ct++] = -1;
			return new_set;
		}	
	}
	
}

/*
 * Interpreter for the destination language.
 * The output here is a list of ids of variables that the regex produces.
 */
int[MAX_VAR] interpretDst(dstExpr s) {
	if (s == null) return ILL_FORMED;	
	switch(s) {
		case VarD: {
			if (s.id <= 0) return ILL_FORMED;
			return {s.id};
		}
		case HoleD: {
			return { };
		}
		case IteD: {
			dstExpr cond = s.cond;
			switch (cond) {
				case HoleD: {
					int[MAX_VAR] new_set;
					int ct = 0;
					int[MAX_VAR] a_set = interpretDst(s.a);
					int[MAX_VAR] b_set = interpretDst(s.b);
					if (a_set == ILL_FORMED || b_set == ILL_FORMED) return ILL_FORMED;
			
					for (int i = 0; i < MAX_VAR; i++) {
						if (a_set[i] != 0) {
							new_set[ct++] = a_set[i];
						} 
					} 
					for (int i = 0; i < MAX_VAR; i++) {
						if (b_set[i] != 0) {
							new_set[ct++] = b_set[i];
						} 
					}
					return new_set; 
				}
				case BoolD: {
					if (cond.v) {
						return interpretDst(s.a);
					} else {
						return interpretDst(s.b);
					}
				}
				default: return ILL_FORMED;
			}
			
		}
		case EmptyD: {
			return {-1};
		}	
		case BoolD: {
			return {s.v};	
		}
	}
	
}